<?php
return [
    'success' => [
        'created'=>'تمت الإضافة بنجاح',
        'updated'=>'تم التعديل بنجاح',
        'deleted'=>'تم الحذف بنجاح',
        'success'=>'عملية ناجحة',
    ],
    'error'=> [
        'error'=>'خطأ',
        'not_found'=>'غير موجود',
        'bad_request'=> 'معلومات خاطئة',
        'unauthorized'=>'هذه العملية غير مسموح بها'
    ],
];

