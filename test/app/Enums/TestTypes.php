<?php
namespace App\Enums;
enum TestTypes: string
{
    case   TRUE_FALSE = 'true_false';
    case MCQ = 'multiple_choice';
}
