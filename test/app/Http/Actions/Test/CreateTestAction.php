<?php

namespace App\Http\Actions\Test;

use App\Http\Actions\ActionBase;
use App\Models\Test;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class CreateTestAction implements ActionBase
{

    public function execute(array $data)
    {

        $test = Test::create(
            $data
        );
        $this->logger();
        return $test;

    }

    public function logger()
    {
        Log::info('Test created successfully at:'.Carbon::parse(time())->format('Y-m-d - H:i:s'));
    }
}
