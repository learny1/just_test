<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'number_of_answers'=>$this->number_of_answers,
            'point_of_answers'=>$this->point_of_answers,
            'questions'=>QuestionResource::collection($this->whenLoaded('questions')),
            'results'=>ResultResource::collection($this->whenLoaded('results')),
        ];
    }
}
