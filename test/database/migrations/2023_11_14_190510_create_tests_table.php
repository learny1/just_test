<?php

use App\Enums\TestTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        Schema::create('tests', function (Blueprint $table) {
            $table->id();
            $table->string('name',125);
            $table->text('description');
            $table->enum('type',[TestTypes::TRUE_FALSE->value,TestTypes::MCQ->value]);
            $table->tinyInteger('number_of_answers');
            $table->json('point_of_answers');
            $table->timestamps();

        });
        DB::statement('ALTER TABLE tests ADD CHECK (number_of_answers = JSON_LENGTH(point_of_answers))');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tests');
    }
};
